# in this exercise author is teaching string formatting in python2

#string format
formatter = "%r %r %r %r"

#print formatted strings
print formatter % (1, 2, 3, 4)
print formatter % ("one", "two", "three", "four")
print formatter % (True, False, True, True)
print formatter % (formatter, formatter, formatter, formatter)
print formatter % (
                   "i am Adnan.",
                   "20 years old.",
                   "aka ElytrA8",
                   "goodbye :-)"
)
