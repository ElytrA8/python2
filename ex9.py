# -- coding: utf-8 --

# define variables
days = "Mon tue wed thu fri sat sun"
months = "Jan\nfeb\nmar\napr\nmay\njun\njul\naug\nsep\noct"

# \n is for newlines
print "here are the days of week :" , days 
print "here are months :" , months

# here """ """ just exactly prints paras just like here 
print """
hello this is me ElytrA8
    this text is formatted 

bye
"""
