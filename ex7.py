# this line defines the code uses utf-8 formatting
# -- coding: utf-8 --

#a random poem 
print "ba ba black sheep"
print "have you any %s" % 'wool'
print "yesir yesir, three bags full"
print "." * 15

#some random variable
a = "M"
b = "e"
c = "a"
d = "t"

e = "B"
f = "u"
g = "r"
h = "g"
i = "e"
j = "r"

# (,) comma here can be used for not breaking the line 
print a + b + c + d,
print e + f + g + h + i + j
